"""
Test the Norad Python SDK against a live HTTP server.
"""
import os.path
import unittest
try:
    from ConfigParser import RawConfigParser
except ImportError:
    from configparser import RawConfigParser

import requests

from norad import Client


# Path to the test configuration file
CONFIG_FILE_PATH = os.path.join(os.path.dirname(__file__), 'config.ini')


class NoradTestCase(unittest.TestCase):
    """ Exercise Norad's Python SDK.
    """

    @classmethod
    def setUpClass(cls):
        """ Initialize the Norad API client.
        """
        config = RawConfigParser()
        config.read(CONFIG_FILE_PATH)
        token = config.get('tests', 'token')
        base_url = config.get('tests', 'url')
        org_name = config.get('tests', 'organization')
        cls.client = Client(token=token, base_url=base_url)
        cls.organization = cls.client.organization(org_name)
        cls.initialize_organization()

    @classmethod
    def initialize_organization(cls):
        try:
            cls.organization.create()
        except requests.exceptions.HTTPError as error:
            template = ('Failed to create test organization "{0}". '
                        'Do you need to update "{1}"?')
            message = template.format(cls.organization['uid'], CONFIG_FILE_PATH)
            error.args = (message,)
            raise error

    @classmethod
    def tearDownClass(cls):
        """ Clean up: delete the test organization.
        """
        cls.organization.delete()

    def test_organization_list_machines(self):
        """ List all machines in an organization.
        """
        machine = self.organization.create_machine(ip='192.168.1.1')
        try:
            machines = self.organization.list_machines()
            self.assertEqual(len(machines), 1)
        finally:
            machine.delete()
        machines = self.organization.list_machines()
        self.assertEqual(len(machines), 0)

    def test_organization_container_configs(self):
        """ Test security container configs at the organization level.
        """
        org = self.organization
        machine = org.create_machine(ip='192.168.2.1')
        try:
            start_len = len(org.list_container_configs())
            config = org.create_container_config('sslyze-heartbleed')
            self.assertEqual(len(org.list_container_configs()), start_len + 1)
            config.delete()
            self.assertEqual(len(org.list_container_configs()), start_len)
        finally:
            machine.delete()

    def test_machine_container_configs(self):
        """ Test security container configs at the machine level.
        """
        machine = self.organization.create_machine(ip='192.168.2.1')
        try:
            config = machine.create_container_config('sslyze-heartbleed')
            self.assertEqual(len(machine.list_container_configs()), 1)
            config.delete()
            self.assertEqual(len(machine.list_container_configs()), 0)
        finally:
            machine.delete()

    @unittest.skip('Black box results do not show up instantly. '
                   'Avoid transient test failures.')
    def test_scan_machine(self):
        """ Configure and scan a machine.
        """
        test_name = 'sslyze-heartbleed'
        machine = self.organization.create_machine(ip='192.168.3.1')
        try:
            machine.create_container_config(test_name)
            machine.scan()

            import time; time.sleep(5)

            found = False
            for result in machine.black_box(latest=True):
                if test_name in result['title']:
                    found = True
            self.assertTrue(found)
        finally:
            machine.delete()

    @unittest.skip('Black box results do not show up instantly. '
                   'Avoid transient test failures.')
    def test_scan_organization(self):
        """ Configure and scan an organization.
        """
        org = self.organization
        test_name = 'sslyze-heartbleed'
        machine = org.create_machine(ip='192.168.3.1')
        try:
            org.create_container_config(test_name)
            org.scan()

            import time; time.sleep(5)

            found = False
            for result in machine.black_box(latest=True):
                if test_name in result['title']:
                    found = True
            self.assertTrue(found)
        finally:
            machine.delete()
